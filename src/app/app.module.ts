import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';

import {DataService} from './services/data.service';
import { AboutComponent } from './components/about/about.component';
import {AUTH_PROVIDERS} from 'angular2-jwt';
import {AuthService} from './services/auth.service';
import { CallbackComponent } from './components/callback/callback.component';
import { AuthGuard } from './auth.guard';

const appRoutes: Routes = [
  {path: 'user', 
  component:UserComponent,
  canActivate: [AuthGuard]},
  {path: 'callback', component:CallbackComponent},
  {path: '', component:AboutComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    AboutComponent,
    CallbackComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataService, AUTH_PROVIDERS, AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
