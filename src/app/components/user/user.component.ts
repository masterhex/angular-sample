import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  idToken:any;
  profile:any;

  constructor(private dataService:DataService, private auth:AuthService) {
    this.idToken = localStorage.getItem("id_token");
    //this.profile = localStorage.getItem("profile");
  }

  ngOnInit() {

  }

}
