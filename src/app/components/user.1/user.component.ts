import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name:string;
  address:Address;
  hobbies:string[];
  posts:Post[];

  constructor(private dataService:DataService, private auth:AuthService) { }

  ngOnInit() {
    this.name = "John Doe";
    this.hobbies = ['asd','sdsd','sdsd','sdsdsd'];
    this.address = {
      street : "13th",
      city: "Okhlohoma city",
      state:"Okhlohoma"
    }
    // this.dataService.getPosts().subscribe((posts) => {
    //   // console.log(posts);
    //   this.posts = posts;
    // })
  }
  onClick(hobby){
    this.hobbies.push(hobby);
  }

}

interface Address{
  street:string,
  city:string,
  state:string
}

interface Post{
  id: number,
  title: string,
  body: string,
  userId: number
}