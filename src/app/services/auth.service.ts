import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import * as auth0 from 'auth0-js';
import { tokenNotExpired } from 'angular2-jwt';

declare var Auth0Lock: any;

@Injectable()
export class AuthService {
    // lock = new Auth0Lock('81DAGRUEiisZPrHL4Q7SSIPSenzCIDT4', 'masterhex.eu.auth0.com', {
    //     auth: {
    //         redirectUrl: 'http://localhost:4200/callback',
    //         responseType: 'token id_token',
    //         params: {
    //             scope: 'openid email' // Learn about scopes: https://auth0.com/docs/scopes
    //         }
    //     }
    // });

    // constructor() {
    //     this.lock.on("authenticated", (authResult: any) => {
    //         this.lock.getUserInfo(authResult.accessToken, function (error, profile) {
    //             if (error) {
    //                 // Handle error
    //                 return;
    //             }
                
    //             localStorage.setItem('id_token', authResult.idToken);
    //             localStorage.setItem('accessToken', authResult.accessToken);
    //             localStorage.setItem('profile', JSON.stringify(profile));
    //         });
    //     });
    // }

    // public login() {
    //     this.lock.show();
    // }

    // public authenticated() {

    //     return tokenNotExpired();
    // }

    // public logout() {
    //     localStorage.removeItem("id_token");
    // }

      auth0 = new auth0.WebAuth({
        clientID: '9F0pTj8on7QpIYR2zuQteIxuHD9l5y1J',
        domain: 'masterhex.eu.auth0.com',
        responseType: 'token id_token',
        audience: 'https://masterhex.eu.auth0.com/userinfo',
        redirectUri: 'http://localhost:4200/callback',
        scope: 'openid email'
      });
      constructor(public router: Router) {}

      public login(): void {
        this.auth0.authorize();
      }
      public handleAuthentication(): void {
        this.auth0.parseHash((err, authResult) => {
          if (authResult && authResult.accessToken && authResult.idToken) {
            window.location.hash = '';
            // auth0.getProfile(authResult.idToken, function(err:any,profile:any){
            //     if(err){
            //         throw new Error(err);
            //     }
            //     localStorage.setItem('profile', JSON.stringify(profile));
            // });
            this.setSession(authResult);
            this.router.navigate(['/']);
          } else if (err) {
            this.router.navigate(['/']);
            console.log(err);
          }
        });
      }

    private setSession(authResult): void {
        // Set the time that the Access Token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
    }

      public logout(): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
      }

      public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // Access Token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
      }


}